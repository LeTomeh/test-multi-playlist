exports.Folder = function(path, serverPath) {
	this.name = null;
	this.serverPath = serverPath;
	this.path = path;
	this.contents = [];
}