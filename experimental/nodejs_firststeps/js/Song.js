function Song() {
	this.service = null;
	this.link = null;
}

function YouTubeSong(link) {
	Song.call(this);

	this.service = "youtube";
	this.link = link;
}

function SoundCloudSong(link) {
	Song.call(this);

	this.service = "soundcloud";
	this.link = link;
}

function LocalSong(link) {
	Song.call(this);

	this.service = "local";
	this.link = link; //server path
}