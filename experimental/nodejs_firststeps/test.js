var express = require('express');
var http = require('http');
var io = require('socket.io');
var fs = require("fs");
var name = "EZ";
var port = 8222;
var path = require("path");

var builder = require('./js/EnvBuilder.js');
var envBuilder = new builder.EnvBuilder();
envBuilder.getFolders();
console.log("booting web server");

var app = express();
app.use('/', express.static('./'));
app.use('/music', express.static('C:\\Users\\lucas\\Desktop\\music'));
app.use('/dl', express.static('C:\\Users\\Lucas\\Downloads'));
//Specifying the public folder of the server to make the html accesible using the static middleware
 
var server =http.createServer(app).listen(port);
//Server listens on the port 8124
io = io.listen(server); 
/*initializing the websockets communication , server instance has to be sent as the argument */
console.log("web server booted and listening on port " + port);
 
io.sockets.on("connection",function(socket){
      console.log('Socket.io : Connection with client established');
      
      console.log('Fs : Reading playlists/playlist.json file');
      var contents = fs.readFileSync("./playlists/playlist.json");
      var util = require('util');
      var request = {
        type: "init", 
        playlist: contents.toString(),
        folders: JSON.stringify(envBuilder.folders)
      }
      console.log(JSON.stringify(request));
      console.log('Socket.io : Sending init packet')
      socket.send(JSON.stringify(request)); 
    
    socket.on("message",function(request){

        request = JSON.parse(request);

        fs.writeFile("./playlists/playlist.json", JSON.stringify(request.data), function(err) {
            if(err) {
              return console.log('Fs : ' + err);
            }
        
            console.log("The file was saved!");
        });
    });
 
});