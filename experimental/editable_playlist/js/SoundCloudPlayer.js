function SoundCloudPlayer(masterPlayer, soundcloudPlayerElement) {
	ServicePlayer.call(this, masterPlayer, soundcloudPlayerElement);
	this.widget = null;
}

SoundCloudPlayer.prototype = Object.create(ServicePlayer.prototype);
SoundCloudPlayer.prototype.constructor = SoundCloudPlayer;

SoundCloudPlayer.prototype.playSong = function(song) {
	if (!this.initialized) {
		this.init(song);
	}
	else {
		this.loadSong(song);
	}
}

SoundCloudPlayer.prototype.init = function(song) {
	this.playerElement.src = "https://w.soundcloud.com/player/?url="+song.link+"&amp;auto_play=false&amp;hide_related=true&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"
	this.widget = SC.Widget(this.playerElement);
	this.widget.bind.call(this.widget, SC.Widget.Events.READY, this.playerReady.bind(this));
}


SoundCloudPlayer.prototype.loadSong = function(song) {
	this.widget.load(song.link, {visual : true, callback : this.play.bind(this)});
}

SoundCloudPlayer.prototype.pause = function() {
	this.widget.pause();
}

SoundCloudPlayer.prototype.toggle = function() {
	this.widget.toggle();
}

// CALLBACKS FOR SOUNDCLOUD PLAYER

SoundCloudPlayer.prototype.songIsPlaying = function () {
	this.widget.getCurrentSound.call(this.widget, function (sound) {
	    console.log('Soundcloud player -> song playing : ' + sound.title);
	});
}

SoundCloudPlayer.prototype.songIsOver = function () {
	console.log('Soundcloud player -> song finished');
	this.masterPlayer.songIterator++;
	this.masterPlayer.play();
}

SoundCloudPlayer.prototype.playerReady = function() {
	console.log('Soundcloud player -> init() -> player initialized');
	this.initialized = true;
	this.widget.bind(SC.Widget.Events.PLAY, this.songIsPlaying.bind(this));
	this.widget.bind(SC.Widget.Events.FINISH, this.songIsOver.bind(this));
	console.log('Soundcloud player -> init() -> play');
	this.widget.play();
}

SoundCloudPlayer.prototype.play = function() {
	console.debug('Soundcloud player -> loadSong() -> song loaded, playing');
	this.widget.play();
}
