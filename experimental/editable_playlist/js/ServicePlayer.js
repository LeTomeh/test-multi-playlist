function ServicePlayer(masterPlayer, servicePlayerElement) {
	this.playerElement = servicePlayerElement;
	this.masterPlayer = masterPlayer;
	this.initialized = false;
}

ServicePlayer.prototype.hide = function() {
	this.playerElement.style.display = 'none';
	console.debug("hide")
}

ServicePlayer.prototype.show = function() {
	this.playerElement.style.display = 'block';
	console.debug("show")
}

ServicePlayer.prototype.playSong = function() {
	throw 'has to be implemented';
}