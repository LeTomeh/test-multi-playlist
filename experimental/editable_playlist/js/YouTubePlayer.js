function YouTubePlayer(playerMaster, youtubePlayerElement) {
	ServicePlayer.call(this, playerMaster, youtubePlayerElement);
	this.widget = null;
	this.isPlaying = false;
}

YouTubePlayer.prototype = Object.create(ServicePlayer.prototype);
YouTubePlayer.prototype.constructor = YouTubePlayer;

YouTubePlayer.prototype.playSong = function(song) {
	console.log('playsong')
	if (!this.initialized) {
		this.init(song);
	}
	else {
		this.loadSong(song);
	}
}

YouTubePlayer.prototype.init = function(song) {
	this.widget = new YT.Player(this.playerElement, {
	  height: '400',
	  width: '400',
	  videoId: this.getVideoIdFromUrl(song.link),
	  playerVars : {
            'autoplay' : 1,
            'enablejsapi' : 1,
        },
	  events: {
	    'onReady': this.playerReady.bind(this),
	    'onStateChange': this.playerStateChange.bind(this)
	  }
	});
	this.playerElement = document.getElementById('youtubePlayer');
}

YouTubePlayer.prototype.loadSong = function(song) {
	console.log("loadsong");
	this.widget.loadVideoById(this.getVideoIdFromUrl(song.link));
}

YouTubePlayer.prototype.getVideoIdFromUrl = function(url) {
	var video_id = url.split('v=')[1];
	var ampersandPosition = video_id.indexOf('&');
	if(ampersandPosition != -1) {
	  video_id = video_id.substring(0, ampersandPosition);
	}
	return video_id;
}

YouTubePlayer.prototype.pause = function() {
	this.widget.pauseVideo();
}

YouTubePlayer.prototype.toggle = function() {
	console.log(this.widget);
	if (this.isPlaying == true)
		this.widget.pauseVideo();
	else {
		console.debug("play videoooo")
		this.widget.playVideo();
		console.debug("play video !!")
	}
}

// CALLBACKS FOR YOUTUBE PLAYER

YouTubePlayer.prototype.playerReady = function(event) {
	console.log('Youtube player -> init() -> player initialized');
	this.initialized = true;
}

YouTubePlayer.prototype.playerStateChange = function(event) {
	if(event.data === 0) //if video is over
	{
		this.isPlaying = false;
		console.log('Youtube player -> init() -> song finished');
		this.masterPlayer.songIterator++;
		this.masterPlayer.play();
	}
	else if(event.data === 1) 
	{
		this.isPlaying = true;
		console.log('playing')
	}
	else if(event.data === 2) 
	{
		this.isPlaying = false;
		console.log('paused')
	}
}