function Player(youtubePlayerElement, soundcloudPlayerElement) {
	this.youtubePlayer = new YouTubePlayer(this, youtubePlayerElement);
	this.soundcloudPlayer = new SoundCloudPlayer(this, soundcloudPlayerElement);
	this.playlist = null;
	this.songIterator = 0;
	this.players = {
		'youtube' : {'play' : this.youtubePlayer.playSong, 'player' : this.youtubePlayer },
		'soundcloud' : {'play' : this.soundcloudPlayer.playSong, 'player' : this.soundcloudPlayer }
	};
	this.services = ['youtube', 'soundcloud'];
}

Player.prototype.play = function() {
	console.log("play method >>>>");
	if (this.songIterator == this.playlist.songs.length) {
		console.log('playlist ended !')
		return;
	}
	var song = this.playlist.songs[this.songIterator];
	console.debug(song)
	this.hideAllExcept(song.service);
	this.players[song.service]['play'].call(this.players[song.service]['player'], song);
}

Player.prototype.loadPlaylist = function(playlist) {
	this.playlist = playlist;	
	this.songIterator = 0;
}

Player.prototype.hideAllExcept = function(playerService) {
	for (var i = 0; i < this.services.length; i++) {
		if (playerService == this.services[i])
			this.players[this.services[i]]['player'].show();
		else
			this.players[this.services[i]]['player'].hide();
	}
}