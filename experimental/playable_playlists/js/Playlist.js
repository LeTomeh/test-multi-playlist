function Playlist() {
	this.songs = null;
}

Playlist.prototype.addSong = function(song) {
	if (this.songs == null) {
		this.songs = [song];
	}
	else {
		this.songs = this.songs.concat([song]);
	}
}

Playlist.prototype.toString = function() {
	if (this.songs != null) {
		var playlist = "";
		for (var i = 0 ; i < this.songs.length ;  i++) {
			playlist += "Song "+(i+1)+": "+this.songs[i].link+" (from "+this.songs[i].service+")<br>";
		}
		return playlist;
	}
}