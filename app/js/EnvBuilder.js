exports.EnvBuilder = function() {
	this.fs = require("fs");
	this.path = require("path");
	this.builderFolder = require('./Folder.js');
	this.builderSong = require('./Song.js');

	this.folders = null;
	this.playlists = null;

	this.getFolders = function() {
		this.folders = [];
		this.folders.push(this.getFolderContent('C:\\Users\\lucas\\Desktop\\music', ''));
		var util = require('util');
	}

	this.getFolderContent = function(serverPath, prefix) {
		serverPathSplitted = serverPath.split("\\");
		var path = serverPathSplitted[serverPathSplitted.length - 1];
  		var folder = new this.builderFolder.Folder(prefix + path, serverPath);
  		folder.name = path;
  		this.fs.readdirSync(serverPath).filter(function(file) {
    		if (this.fs.statSync(this.path.join(serverPath, file)).isDirectory())
    		{
    			folder.contents.push(this.getFolderContent(this.path.join(serverPath, file), 
    				prefix + path + "/"));
    		}
    		else
    		{
				var tmpSong = {
      				path: serverPath, 
      				link: prefix + path + "/" + file,
      				name: file
      			}
    			//folder.contents.push(this.path.join(serverPath, file));
    			folder.contents.push(tmpSong);
    		}
  		}.bind(this));
  		return folder;
	}
}