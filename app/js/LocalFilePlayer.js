function LocalFilePlayer(masterPlayer) {
	ServicePlayer.call(this, masterPlayer);
	this.sound = null; 
	this.widget = null;
}

LocalFilePlayer.prototype = Object.create(ServicePlayer.prototype);
LocalFilePlayer.prototype.constructor = LocalFilePlayer;

LocalFilePlayer.prototype.playSong = function(song) {
soundManager.setup({
  url: '/path/to/swf-files/',
  onready: function() {
	this.sound = soundManager.createSound({
      url: song.link,
      onfinish: function() { songIsOver(); }
    });
    this.sound.play();
  }.bind(this),
  ontimeout: function() {
    // Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
  }
});
}

LocalFilePlayer.prototype.loadSong = function(song) {
	soundManager.setup({
  url: '/path/to/swf-files/',
  onready: function() {
    this.sound = soundManager.createSound({
      id: 'wtfckoica',
      url: song.link
    });
  },
  ontimeout: function() {
    // Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
  }
});
	this.sound.onfinish = songIsOver;
}

SoundCloudPlayer.prototype.songIsOver = function () {
	console.log('Soundcloud player -> song finished');
	this.masterPlayer.songIterator++;
	this.masterPlayer.play();
}

LocalFilePlayer.prototype.pause = function() {
	this.sound.pause();
}

LocalFilePlayer.prototype.toggle = function() {
	this.sound.togglePause();
}