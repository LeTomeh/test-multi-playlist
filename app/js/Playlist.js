function Playlist() {
	this.songs = null;
}

Playlist.prototype.addSong = function(song) {
	if (this.songs == null) {
		this.songs = [song];
	}
	else {
		this.songs = this.songs.concat([song]);
	}
}

Playlist.prototype.setSongs = function(songs) {
	this.songs = songs;
}

Playlist.prototype.toString = function() {
	if (this.songs != null) {
		var playlist = "";
		console.log(this.songs.length);
		for (var i = 0 ; i < this.songs.length ;  i++) {
			playlist += "<li class=\"song\">Song "+(i+1)+": "+this.songs[i].link+" (from "+this.songs[i].service+")</li>";
		}
		return playlist;
	}
	return "empty";
}